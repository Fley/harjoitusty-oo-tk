/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: createNewWindow.java
 * Author: Otto Itkonen
 */

/*
 * Opens a new window with specified parameters.
 */

package harjoitustyo;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author k2002
 */
public class createNewWindow {
    private Stage stage;
    
    public createNewWindow(String fxmlName, String title, boolean resize) {
        try {
            FXMLLoader floader = new FXMLLoader(getClass().getResource(fxmlName));
            Parent root = (Parent) floader.load();
            stage = new Stage();
            
            stage.setTitle(title);
            stage.setScene(new Scene(root));
            stage.show();
            stage.setResizable(resize);
            stage.centerOnScreen();
            stage.requestFocus();
        } catch(IOException e) {
            System.err.println("IOException caught: " + e.getMessage());
        }
    }
    
    public Stage getStage() {
        return stage;
    }
}
