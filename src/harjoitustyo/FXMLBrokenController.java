/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: FXMLBrokenController.java
 * Author: Otto Itkonen
 */

/*
 * Controller for a notification message window if sent 
 * package has broken on its way to its destination.
 */

package harjoitustyo;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author k2002
 */
public class FXMLBrokenController implements Initializable {
    @FXML
    private Button okButton;

    @Override
    public void initialize(URL url, ResourceBundle rb) {}    

    @FXML
    private void okButtonAction(ActionEvent event) {
        Stage stage = (Stage) okButton.getScene().getWindow();
        
        stage.close();
    }
    
}
