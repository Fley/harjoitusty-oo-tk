/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: PackageStorage.java
 * Author: Otto Itkonen
 */

/*
 * Package storage class to store created packages and move them into
 * the database. Also houses subclasses PackageInfo and PackageInfoExtended
 * which are used to create ArrayLists of current storage to give away
 * to other classes.
 */

package harjoitustyo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author k2002
 */
public class PackageStorage {
    private Connection con;
    private Statement stat;
    private String insertInto;
    private int itemID;
    private int classNmb;
    private static ArrayList<PackageInfo> storaged;
    private static ArrayList<PackageInfoExtended> storagedExt;
    
    public PackageStorage() {
        con = null;
        stat = null;
    }
    
    public PackageStorage(boolean firstTimeInitialization) {
        con = null;
        stat = null;
        
        storaged = new ArrayList<>();
        storagedExt = new ArrayList<>();
    }
    
    /*
     * Takes a package as a parameter and inserts it into the database.
     */
    public void addPackToStor(Package pk) {
        itemID = pk.getPackagedItem();
        classNmb = pk.getPackageClass();
        
        insertInto = "INSERT INTO Package (itemid, classid) " +
                "VALUES ('" + itemID + "', '" + classNmb + "');";
        
        try{
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:timotei.db");
            
            stat = con.createStatement();
            stat.executeUpdate(insertInto);
            
            stat.close();
            con.close();
        } catch(ClassNotFoundException e) {
            System.err.println("ClassNotFoundException caught: " + e.getMessage());
        } catch(SQLException e) {
            System.err.println("SQLException caught: " + e.getMessage());
        }
    }
    
    public static PackageStorage newPkgStorage() {
        return new PackageStorage(true);
    }
    
    public ArrayList<PackageInfo> getStoraged() {
        return storaged;
    }
    
    public ArrayList<PackageInfoExtended> getStoragedExtended() {
        return storagedExt;
    }
    
    /*
     * Updates the "storaged" ArrayList with current storage info
     * and returns the list.
     */
    public ArrayList<PackageInfo> getUpdatedStorage() {
        storaged.clear();
        
        String search = "SELECT * FROM Storaged;";
            
        try {
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:timotei.db");
            con.setAutoCommit(false);
            
            stat = con.createStatement();
            ResultSet rs = stat.executeQuery(search);
            
            while(rs.next()) {
                int pkgID = rs.getInt("Package number");
                int cID = rs.getInt("Class");
                String iName = rs.getString("Item");
                
                PackageInfo pi = new PackageInfo(iName, pkgID, cID);
                storaged.add(pi);
            }
            
            con.commit();
            con.setAutoCommit(true);
            
            rs.close();
            stat.close();
            con.close();
        } catch(ClassNotFoundException e) {
            System.err.println("ClassNotFoundException caught: " + e.getMessage());
        } catch(SQLException e) {
            System.err.println("SQLException caught: " + e.getMessage());
        }
        
        return storaged;
    }
    
    /*
     * Updates the "storageExt" ArrayList with current storage's extended
     * info and returns the list.
     */
    public ArrayList<PackageInfoExtended> getUpdatedExtendedStorage() {
        storagedExt.clear();
        
        String search = "SELECT * FROM StoragedExtended;";
        
        try {
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:timotei.db");
            con.setAutoCommit(false);
            
            stat = con.createStatement();
            ResultSet rs = stat.executeQuery(search);
            
            while(rs.next()) {
                int pkgID = rs.getInt("Package number");
                int cID = rs.getInt("Class");
                String iName = rs.getString("Item");
                double size = rs.getDouble("Size");
                double weight = rs.getDouble("Weight");
                
                PackageInfoExtended pix;
                pix = new PackageInfoExtended(iName, pkgID, cID, size, weight);
                storagedExt.add(pix);
            }
            
            con.commit();
            con.setAutoCommit(true);
            
            rs.close();
            stat.close();
            con.close();
        } catch(ClassNotFoundException e) {
            System.err.println("ClassNotFoundException caught: " + e.getMessage());
        } catch(SQLException e) {
            System.err.println("SQLException caught: " + e.getMessage());
        }
        
        return storagedExt;
    }
    
    /*
     * Subclass to create infobundles of stored packages for display 
     * and use elsewhere.
     */
    public class PackageInfo {
        private final String itemName;
        private final int packageID;
        private final int classID;
        
        public PackageInfo(String item, int pkgID, int cID) {
            itemName = item;
            packageID = pkgID;
            classID = cID;
        }
        
        public int getPackageID() {
            return packageID;
        }
        
        public int getPackageClass() {
            return classID;
        }
        
        @Override
        public String toString() {
            return "ID: " + packageID + ", " + '"' + itemName + '"' 
                    + ", Class: " + classID;
        }
    }
    
    /*
     * Subclass to create extended infobundles of stored packages that also
     * include package's size and weight. For display and use elsewhere.
     */
    public class PackageInfoExtended {
        private final String itemName;
        private final int packageID;
        private final int classID;
        private final double itemSize;
        private final double itemWeight;
        
        public PackageInfoExtended(String item, int pkgID, int cID, double size, double weight) {
            itemName = item;
            packageID = pkgID;
            classID = cID;
            itemSize = size;
            itemWeight = weight;
        }
        
        @Override
        public String toString() {
            return "ID: " + packageID + ", Item: " + '"' + itemName + '"' 
                    + ", Class: " + classID + "\n\tSize: " + itemSize 
                    + " l,  Weight: " + itemWeight + " kg";
        }
    }
}
