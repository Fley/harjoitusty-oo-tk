/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: FXMLNotDoubleController.java
 * Author: Otto Itkonen
 */

/*
 * Controller for a warning window for when given 
 * size and weight are not in double format.
 */

package harjoitustyo;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author k2002
 */
public class FXMLNotDoubleController implements Initializable {
    @FXML
    private Button okButton;

    @Override
    public void initialize(URL url, ResourceBundle rb) {}    

    @FXML
    private void okButtonAction(ActionEvent event) {
        Stage stage = (Stage) okButton.getScene().getWindow();
        
        stage.close();
    }
    
}
