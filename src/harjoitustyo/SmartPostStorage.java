/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: SmartPostStorage.java
 * Author: Otto Itkonen
 */

/*
 * Storage class for SmartPosts and their GeoPoints.
 * Stores all info related to SPs and GPs and is
 * also responsible for moving that info into the
 * database.
 */

package harjoitustyo;

import harjoitustyo.SmartPost.GeoPoint;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author k2002
 */
public class SmartPostStorage {
    private static ArrayList<SmartPost> spOnMap;
    private static ArrayList<String> pCodeList;
    private static ArrayList<String> cityList;
    private String insertInto;
    private String readFrom;
    private Connection con;
    private Statement stat;
    private int spID;
    private String latitude;
    private String longitude;
    private String postoffice;
    private String open;
    private String address;
    private String postcode;
    private String city;
    
    public SmartPostStorage() {
        spOnMap = new ArrayList<>();
        pCodeList = new ArrayList<>();
        cityList = new ArrayList<>();
    }
    
    /*
     * Inserts given GeoPoint into the database.
     */
    public int addGPToDB(GeoPoint gp) {
        latitude = gp.getGPLat();
        longitude = gp.getGPLon();
        
        insertInto = "INSERT INTO Coordinates (latitude, longitude) " +
                "VALUES ('" + latitude + "', '" + longitude + "');";
        readFrom = "SELECT smartpostid FROM Coordinates " +
                "WHERE latitude LIKE '" + latitude + "' AND " +
                "longitude LIKE '" + longitude + "';";
        
        try{
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:timotei.db");
            con.setAutoCommit(false);
            
            stat = con.createStatement();
            stat.executeUpdate(insertInto);
            con.commit();
            
            /*
             * Looks for the smartpostid of the GeoPoint that
             * was just added to the database, so that it can
             * be returned and the GeoPoint's location inside
             * the database is known.
             */
            ResultSet rs = stat.executeQuery(readFrom);
            spID = rs.getInt("smartpostid");
            con.commit();
            
            con.setAutoCommit(true);
            
            rs.close();
            stat.close();
            con.close();
        } catch(ClassNotFoundException e) {
            System.err.println("ClassNotFoundException caught: " + e.getMessage());
        } catch(SQLException e) {
            System.err.println("SQLException caught: " + e.getMessage());
        }
        return spID;
    }
    
    /*
     * Inserts given SmartPost automaton into the database.
     */
    public void addSPToDB(int smartpostid, SmartPost sp) {
        postcode = sp.getPostCode();
        address = sp.getAddress();
        city = sp.getCity();
        postoffice = sp.getPostOffice();
        open = sp.getAvailability();
        
        
        try{
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:timotei.db");
            con.setAutoCommit(false);
            
            stat = con.createStatement();
            
            insertInto = "INSERT INTO Info VALUES (" + smartpostid +
                    ", '" + postoffice + "', '" + open + "');";
            stat.executeUpdate(insertInto);
            
            insertInto = "INSERT INTO Address VALUES (" + smartpostid +
                    ", '" + address + "');";
            stat.executeUpdate(insertInto);
            
            insertInto = "INSERT INTO Postcode VALUES (" + smartpostid +
                    ", '" + postcode + "');";
            stat.executeUpdate(insertInto);
            
            if(!getPostCodes().contains(postcode)) {
                insertInto = "INSERT INTO City VALUES ('" + postcode + 
                        "', '" + city.toUpperCase() + "');";
                stat.executeUpdate(insertInto);
                addToPostCodes(postcode);
                
                if(!getCities().contains(city.toUpperCase()))
                    addToCities(city.toUpperCase());
            }
            
            con.commit();
            con.setAutoCommit(true);
            
            stat.close();
            con.close();
        } catch(ClassNotFoundException e) {
            System.err.println("ClassNotFoundException caught: " + e.getMessage());
        } catch(SQLException e) {
            System.err.println("SQLException caught: " + e.getMessage());
        }
    }
    
    public static void addToPostCodes(String postcode) {
        pCodeList.add(postcode);
    }
    
    public static void addToCities(String city) {
        cityList.add(city);
    }
    
    public static void addToSPsOnMap(SmartPost sp) {
        spOnMap.add(sp);
    }
    
    public static ArrayList<String> getPostCodes() {
        return pCodeList;
    }
    
    public static ArrayList<String> getCities() {
        return cityList;
    }
    
    public static ArrayList<SmartPost> getSPsOnMap() {
        return spOnMap;
    }
    
}
