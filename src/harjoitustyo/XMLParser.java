/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: XMLParser.java
 * Author: Otto Itkonen
 */

/* 
 * Takes XML-content as a parameter and parses it.
 */

package harjoitustyo;

import java.io.IOException;
import java.io.StringReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author k2002
 */
public class XMLParser {
    private DocumentBuilderFactory documentFactory;
    private DocumentBuilder documentBuilder;
    private Document document;
    
    public XMLParser (String content) {
        try {
            documentFactory = DocumentBuilderFactory.newInstance();
            documentBuilder = documentFactory.newDocumentBuilder();
            
            document = documentBuilder.parse(new InputSource(new StringReader(content)));
            document.getDocumentElement().normalize();
            
        } catch(ParserConfigurationException e) {
            System.err.println("ParserConfigurationException caught: " +  e.getMessage());
        } catch(SAXException e) {
            System.err.println("SAXException caught: " + e.getMessage());
        } catch(IOException e) {
            System.err.println("IOException caught: " + e.getMessage());
        }
    }
    
    public Document getParsedDocument() {
        return document;
    }
}
