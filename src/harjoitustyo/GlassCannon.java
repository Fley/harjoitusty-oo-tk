/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: GlassCannon.java
 * Author: Otto Itkonen
 */

/*
 * A class to create a really fragile Glass Cannon.
 * It will break.
 */

package harjoitustyo;

/**
 *
 * @author k2002
 */
public class GlassCannon extends Item {

    public GlassCannon() {
        super("Glass Cannon", 1020, 3000, true);
    }
}
