/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: PumpkinPkg.java
 * Author: Otto Itkonen
 */

/*
 * A class to package a pumpkin.
 */

package harjoitustyo;

/**
 *
 * @author k2002
 */
public class PumpkinPkg extends Package{

    public PumpkinPkg(int itemID) {
        super(2, itemID);
    }
}
