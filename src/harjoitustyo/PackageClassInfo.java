/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: PackageClassInfo.java
 * Author: Otto Itkonen
 */

/*
 * Looks for all the package classes inside the database and 
 * saves the found data into ClassInfo objects with the ClassInfo
 * subclass, then saves those objects into an ArrayList.
 */

package harjoitustyo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author k2002
 */
public class PackageClassInfo {
    private static ArrayList<ClassInfo> infoList;
    private Connection con;
    private Statement stat;
    
    public PackageClassInfo() {
        infoList = new ArrayList<>();
        
        con = null;
        stat = null;
        
        String search = "SELECT * FROM ClassInfo;";
            
        try {
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:timotei.db");
            con.setAutoCommit(false);
            
            stat = con.createStatement();
            ResultSet rs = stat.executeQuery(search);
            
            while(rs.next()) {
                int id = rs.getInt("Class");
                int days = rs.getInt("Delivery");
                double size = rs.getDouble("MaxSize");
                double weight = rs.getDouble("MaxWeight");
                int dist = rs.getInt("MaxDistance");
                boolean breakage = rs.getBoolean("Breakage");
                
                ClassInfo ci = new ClassInfo(id, days, size, weight, dist, breakage);
                infoList.add(ci);
            }
            
            con.commit();
            con.setAutoCommit(true);
            
            rs.close();
            stat.close();
            con.close();
        } catch(ClassNotFoundException e) {
            System.err.println("ClassNotFoundException caught: " + e.getMessage());
        } catch(SQLException e) {
            System.err.println("SQLException caught: " + e.getMessage());
        }
    }
    
    /*
     * A getter for the class info ArrayList.
     */
    public static ArrayList<ClassInfo> getClassInfo() {
        return infoList;
    }
    
    /*
     * Subclass to store all class info.
     */
    public class ClassInfo {
        private final int id;
        private final int days;
        private final double size;
        private final double weight;
        private final int distance;
        private final boolean breakage;
        
        public ClassInfo(int id, int days, double size, double weight, 
                int distance, boolean breakage) {
            this.id = id;
            this.days = days;
            this.size = size;
            this.weight = weight;
            this.distance = distance;
            this.breakage = breakage;
        }
        
        public int getClassID() {
            return id;
        }
        
        public double getClassSize() {
            return size;
        }
        
        public double getClassWeight() {
            return weight;
        }
        
        @Override
        public String toString() {
            String dayFormat = "days";
            String limits;
            
            if(days == 1)
                dayFormat = "day";
            
            if((size + weight == 0) && distance != 0) {
                limits = "Restrictions:  Distance: " + distance;
            } else if((size + weight != 0) && distance == 0) {
                limits = "Restrictions:  Size: " + size + " l,  Weight: " 
                        + weight + " kg";
            } else if((size + weight != 0) && distance != 0) {
                limits = "Restrictions:  Size: " + size + " l,  Weight: " 
                        + weight + " kg,  Distance: " + distance + " km";
            } else {
                limits = "";
            }
            
            if(breakage == true) {
                return "Class: " + id + "\n\t"
                        + "Delivery in " + days + " " + dayFormat 
                        + ", no fragile items!\n\t" + limits;
            } else {
                return "Class: " + id + "\n\t"
                        + "Delivery in " + days + " " + dayFormat 
                        + "\n\t" + limits;
            }
        }
    }
}
