/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: IkeaStoolPkg.java
 * Author: Otto Itkonen
 */

/*
 * A class to package a stool from Ikea.
 */

package harjoitustyo;

/**
 *
 * @author k2002
 */
public class IkeaStoolPkg extends Package{

    public IkeaStoolPkg(int itemID) {
        super(1, itemID);
    }
}
