/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: SmartPost.java
 * Author: Otto Itkonen
 */

/* 
 * Stores data for a single SmartPost.
 * Subclass stores SmartPost's geographical data.
 */

package harjoitustyo;

/**
 *
 * @author k2002
 */
public class SmartPost {
    private final String postcode;
    private final String city;
    private final String address;
    private final String open;
    private final String postoffice;
    
    public SmartPost(String pcode, String city, 
            String addr, String open, String poffice) {
        postcode = pcode;
        this.city = city;
        address = addr;
        this.open = open;
        postoffice = poffice;
    }
    
    public String getPostCode() {
        return postcode;
    }
    
    public String getCity() {
        return city;
    }
    
    public String getAddress() {
        return address;
    }
    
    public String getAvailability() {
        return open;
    }
    
    public String getPostOffice() {
        return postoffice;
    }
    
    @Override
    public String toString() {
        return postoffice + ", " + city;
    }
    
    public class GeoPoint {
        private final String latitude;
        private final String longitude;
        
        public GeoPoint(String lan, String lon) {
            latitude = lan;
            longitude = lon;
        }
        
        public String getGPLat() {
            return latitude;
        }
        
        public String getGPLon() {
            return longitude;
        }
        
    }
    
}