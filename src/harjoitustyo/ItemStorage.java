/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: ItemStorage.java
 * Author: Otto Itkonen
 */

/*
 * This class takes care of storaging items and moving them
 * in and out of database.
 */

package harjoitustyo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author k2002
 */
public class ItemStorage {
    private static ArrayList<ItemID> itemIDList;
    private static ArrayList<ItemInfo> itemInfoList;
    
    public ItemStorage() {}
    
    /*
     * Takes an item's info as parameter and inserts the item into the database.
     */
    public static void insertItemIntoDB(String name, double size, double weight, boolean fragile) {
        Connection con = null;
        Statement stat = null;
        
        int frag = 0;
        if(fragile == true)
            frag = 1;
        
        try {
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:timotei.db");
            
            stat = con.createStatement();
            
            String content = "INSERT INTO Item (itemname, itemsize, " +
                    "itemweight, fragile) VALUES ('" + name + "', " +
                    size + ", " + weight + ", " + frag + ");";
            
            stat.executeUpdate(content);
            
            stat.close();
            con.close();
        } catch(ClassNotFoundException e) {
            System.err.println("ClassNotFoundException caught: " + e.getMessage());
        } catch(SQLException e) {
            System.err.println("SQLExpection caught: " + e.getMessage());
        }
    }
    
    /*
     * Gets items and their IDs from database and puts them into an
     * ArrayList, then returns the ArrayList.
     */
    public ArrayList<ItemID> getItemIDList() {
        itemIDList = new ArrayList<>();
        
        Connection con = null;
        Statement stat = null;
        
        String search = "SELECT * FROM ItemID;";
            
        try {
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:timotei.db");
            con.setAutoCommit(false);
            
            stat = con.createStatement();
            ResultSet rs = stat.executeQuery(search);
            
            while(rs.next()) {
                int itemid = rs.getInt("ID");
                String itemname = rs.getString("Name");
                
                ItemID iid = new ItemID(itemname, itemid);
                itemIDList.add(iid);
            }
            
            con.commit();
            con.setAutoCommit(true);
            
            rs.close();
            stat.close();
            con.close();
        } catch(ClassNotFoundException e) {
            System.err.println("ClassNotFoundException caught: " + e.getMessage());
        } catch(SQLException e) {
            System.err.println("SQLException caught: " + e.getMessage());
        }
        
        return itemIDList;
    }
    
    /*
     * Gets items' info, creates ItemInfo objects and adds those objects into
     * an ArrayList that it then returns.
     */
    public ArrayList<ItemInfo> getItemInfoList() {
        itemInfoList = new ArrayList<>();
        
        Connection con = null;
        Statement stat = null;
        
        String search = "SELECT * FROM Item;";
            
        try {
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:timotei.db");
            con.setAutoCommit(false);
            
            stat = con.createStatement();
            ResultSet rs = stat.executeQuery(search);
            
            while(rs.next()) {
                int id = rs.getInt("itemid");
                String name = rs.getString("itemname");
                double size = rs.getDouble("itemsize");
                double weight = rs.getDouble("itemweight");
                boolean fragile = rs.getBoolean("fragile");
                
                ItemInfo iinfo = new ItemInfo(id, name, size, weight, fragile);
                itemInfoList.add(iinfo);
            }
            
            con.commit();
            con.setAutoCommit(true);
            
            rs.close();
            stat.close();
            con.close();
        } catch(ClassNotFoundException e) {
            System.err.println("ClassNotFoundException caught: " + e.getMessage());
        } catch(SQLException e) {
            System.err.println("SQLException caught: " + e.getMessage());
        }
        
        return itemInfoList;
    }
    
    /*
     * Subclass for item's name and ID.
     */
    public class ItemID {
        private final int id;
        private final String name;
        
        public ItemID (String name, int id) {
            this.name = name;
            this.id = id;
        }
        
        public int getItemID() {
            return id;
        }
        
        @Override
        public String toString() {
            return "ID: " + id + ", Name: " + '"' + name + '"';
        }
    }
    
    /*
     * Subclass for all of an item's info.
     */
    public class ItemInfo {
        private final int id;
        private final String name;
        private final double size;
        private final double weight;
        private final boolean fragile;
        
        public ItemInfo(int id, String name, double size, double weight, boolean fragile) {
            this.id = id;
            this.name = name;
            this.size = size;
            this.weight = weight;
            this.fragile = fragile;
        }
        
        public int getItemID() {
            return id;
        }
        
        public double getItemSize() {
            return size;
        }
        
        public double getItemWeight() {
            return weight;
        }
        
        @Override
        public String toString() {
            if(fragile == true) {
                return "ID: " + id + ", Name: " + '"' + name + '"' 
                        + ", FRAGILE!\n\tSize: " + size + " l,  Weight: " 
                        + weight + " kg";
            } else {
                return "ID: " + id + ", Name: " + '"' + name + '"' 
                        + "\n\tSize: " + size + " l,  Weight: " 
                        + weight + " kg";
            }
        }
    }
}
