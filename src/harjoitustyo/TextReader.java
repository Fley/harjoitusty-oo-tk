/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: TextReader.java
 * Author: Otto Itkonen
 */

/*
 * Takes a text file name as a parameter and
 * attempts to read it, then saves the 
 * contents to a string.
 */

package harjoitustyo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author k2002
 */
public class TextReader {
    private BufferedReader reader;
    private String content;
    private String line;
    
    public TextReader(String file) {
        content = "";
        
        try {
            reader = new BufferedReader(new FileReader(file));
            
            while((line = reader.readLine()) != null) {
                content += line + "\n";
            }
            
            reader.close();
        } catch(FileNotFoundException e) {
            System.err.println("FileNotFoundException caught: " + e.getMessage());
        } catch(IOException e) {
            System.err.println("IOException caught: " + e.getMessage());
        }
    }
    
    public String getTextContent() {
        return content;
    }
}
