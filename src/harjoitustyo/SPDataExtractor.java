/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: SPDataExctractor.java
 * Author: Otto Itkonen
 */

/*
 * Extracts nodes from a parsed document given
 * as a parameter, then searches for specific 
 * info from those nodes, creates a new 
 * SmartPost object along with a new GeoPoint 
 * object and adds them to the SmartPostStorage.
 */

package harjoitustyo;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author k2002
 */
public class SPDataExtractor {
    private final NodeList nodeList;
    private final Document document;
    private final SmartPostStorage storage;
    
    public SPDataExtractor(Document doc) {
        document = doc;
        nodeList = document.getElementsByTagName("place");
        storage = new SmartPostStorage();
        
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            Element elem = (Element) node;
            
            String code = getValue("code", elem);
            String city = getValue("city", elem);
            String addr = getValue("address", elem);
            String open = getValue("availability", elem);
            String office = getValue("postoffice", elem);
            String lat = getValue("lat", elem);
            String lng = getValue("lng", elem);
            
            SmartPost sp = new SmartPost(code, city, addr, open, office);
            SmartPost.GeoPoint gp = sp.new GeoPoint(lat, lng);
            
            /*
             * Uses SmartPostStorage.java's methods to add 
             * the newly created SmartPost and GeoPoint 
             * objects into the database.
             */
            int spID = storage.addGPToDB(gp);
            storage.addSPToDB(spID, sp);
        }
    }

    private String getValue(String tag, Element e) {
        return e.getElementsByTagName(tag).item(0).getTextContent();
    }
}
