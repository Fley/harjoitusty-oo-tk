/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: Item.java
 * Author: Otto Itkonen
 */

/*
 * A class from which other items are created. Uses 
 * ItemStorage.addItemToStorage to insert an item into the database.
 */

package harjoitustyo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author k2002
 */
public class Item {
    private final String name;
    private final double size;
    private final double weight;
    private final boolean fragile;
    private int id;
    private Connection con;
    private Statement stat;
    
    public Item(String name, double size, double weight, boolean fragile) {
        this.name = name;
        this.size = size;
        this.weight = weight;
        this.fragile = fragile;
        
        /*
         * Inserts the created item into the database.
         */
        ItemStorage.insertItemIntoDB(this.name, this.size, this.weight, this.fragile);
        
        con = null;
        stat = null;
        
        int frag = 0;
        if(fragile == true)
            frag = 1;
        
        String search = "SELECT itemid FROM Item "
                + "WHERE itemname = '" + name + "' AND "
                + "itemsize = " + size + " AND "
                + "itemweight = " + weight + " AND "
                + "fragile = " + frag + ";";
        
        /*
         * Looks for the id assigned to the newly created item.
         */
        try {
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:timotei.db");
            con.setAutoCommit(false);
            
            stat = con.createStatement();
            ResultSet rs = stat.executeQuery(search);
            
            id = rs.getInt("itemid");
            
            con.commit();
            con.setAutoCommit(true);
            
            rs.close();
            stat.close();
            con.close();
        } catch(ClassNotFoundException e) {
            System.err.println("ClassNotFoundException caught: " + e.getMessage());
        } catch(SQLException e) {
            System.err.println("SQLException caught: " + e.getMessage());
        }
    }
    
    public String getItemName() {
        return name;
    }
    
    public double getItemSize() {
        return size;
    }
    
    public double getItemWeight() {
        return weight;
    }
    
    public boolean getItemFragility() {
        return fragile;
    }
    
    public int getItemID() {
        return id;
    }
}
