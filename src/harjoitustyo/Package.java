/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: Package.java
 * Author: Otto Itkonen
 */

/*
 * A class to create new packages from.
 */

package harjoitustyo;

/**
 *
 * @author k2002
 */
public class Package {
    private final int classNmb;
    private final int itemID;
    
    public Package(int nmb, int item) {
        classNmb = nmb;
        itemID = item;
    }
    
    public int getPackagedItem() {
        return itemID;
    }
    
    public int getPackageClass() {
        return classNmb;
    }
}
