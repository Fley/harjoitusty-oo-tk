/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: FXMLItemsInfoController.java
 * Author: Otto Itkonen
 */

/*
 * Controller for the items' info view.
 */

package harjoitustyo;

import harjoitustyo.ItemStorage.ItemInfo;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;

/**
 * FXML Controller class
 *
 * @author k2002
 */
public class FXMLItemsInfoController implements Initializable {
    
    private ItemStorage itemStorage;
    private ArrayList<ItemInfo> items;
    private String content;
    @FXML
    private Label headerLabel;
    @FXML
    private TextArea infoTextArea;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        itemStorage = new ItemStorage();
        
        items = itemStorage.getItemInfoList();
        
        infoTextArea.setEditable(false);
        infoTextArea.setWrapText(false);
        
        content = "";
        
        for (ItemInfo iinfo : items) {
            content = content + iinfo.toString() + "\n\n";
        }
        
        infoTextArea.setText(content);
    }    
    
}
