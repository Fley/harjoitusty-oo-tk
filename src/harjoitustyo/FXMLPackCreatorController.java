/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: FXMLPackCreatorController.java
 * Author: Otto Itkonen
 */

/*
 * Controller class responsible 
 * for the package creator UI.
 */

package harjoitustyo;

import harjoitustyo.ItemStorage.ItemID;
import harjoitustyo.ItemStorage.ItemInfo;
import harjoitustyo.PackageClassInfo.ClassInfo;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author k2002
 */
public class FXMLPackCreatorController implements Initializable {
    
    private ItemStorage itemStorage;
    private ToggleGroup classes;
    private ToggleGroup itemGroup;
    private Stage stage;
    private ArrayList<ClassInfo> classInfoList;
    private ArrayList<ItemInfo> itemInfoList;
    @FXML
    private ComboBox<ItemID> itemBox;
    @FXML
    private TextField itemNameField;
    @FXML
    private TextField itemSizeField;
    @FXML
    private TextField itemWeightField;
    @FXML
    private CheckBox fragileCheck;
    @FXML
    private RadioButton class1radio;
    @FXML
    private RadioButton class2radio;
    @FXML
    private RadioButton class3radio;
    @FXML
    private RadioButton itemExistRadio;
    @FXML
    private RadioButton itemNewRadio;
    @FXML
    private Button createCancelButton;
    @FXML
    private Button createAcceptButton;
    @FXML
    private Button classInfoButton;
    @FXML
    private Button showItemsButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /*
         * Groups up all the radio buttons in appropriate groups
         * and fills itemBox with existing items.
         */
        itemStorage = new ItemStorage();
        
        classes = new ToggleGroup();
        class1radio.setToggleGroup(classes);
        class1radio.setSelected(true);
        class2radio.setToggleGroup(classes);
        class3radio.setToggleGroup(classes);
        
        itemGroup = new ToggleGroup();
        itemExistRadio.setToggleGroup(itemGroup);
        itemExistRadio.setSelected(true);
        itemNewRadio.setToggleGroup(itemGroup);
        
        itemBox.getItems().addAll(itemStorage.getItemIDList());
    }

    @FXML
    private void createCancelAction(ActionEvent event) {
        /*
         * Closes the window without other actions.
         */
        stage = (Stage) createCancelButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void createAcceptAction(ActionEvent event) {
        /*
         * First tests whether user is creating a package from
         * an existing item or creating a new item. If existing
         * item is used, takes item's ID and wanted class' number,
         * creates a new package and inserts into the database.
         * Then closes the window.
         */
        classInfoList = PackageClassInfo.getClassInfo();
        
        if(itemExistRadio.isSelected()) {
            if(itemBox.getValue() != null) {
                int chooseClass;
                if(class1radio.isSelected()) {
                    chooseClass = 1;
                } else if(class2radio.isSelected()) {
                    chooseClass = 2;
                } else {
                    chooseClass = 3;
                }
                
                /*
                 * Look for item's size and weight and compare to
                 * class' max size and max weight.
                 */
                int itemID = itemBox.getValue().getItemID();
                itemInfoList = itemStorage.getItemInfoList();
                
                double size = 0;
                double weight = 0;
                for (ItemInfo iinfo : itemInfoList) {
                    if(iinfo.getItemID() == itemID) {
                        size = iinfo.getItemSize();
                        weight = iinfo.getItemWeight();
                        break;
                    }
                }
                
                double maxSize = 0;
                double maxWeight = 0;
                for (ClassInfo cinfo : classInfoList) {
                    if(cinfo.getClassID() == chooseClass) {
                        maxSize = cinfo.getClassSize();
                        maxWeight = cinfo.getClassWeight();
                        break;
                    }
                }
                
                /*
                 * If the item is smaller than or equal to the class restrictions
                 * or class has no restrictions, the item can be packaged. Else
                 * throw a warning message about an item too big.
                 */
                if((maxSize == 0 && maxWeight == 0) || (maxSize >= size && maxWeight >= weight)) {
                    Package pkg = new Package(chooseClass, itemID);
                    
                    PackageStorage str = new PackageStorage();
                    str.addPackToStor(pkg);
                    
                    stage = (Stage) createAcceptButton.getScene().getWindow();
                    stage.close();
                } else {
                    new createNewWindow("FXMLTooBig.fxml", "Warning", false);
                }
            } else {
                /*
                 * Warning window if no item is selected 
                 * but existing item wanted.
                 */
                new createNewWindow("FXMLNoItem.fxml", "Warning", false);
            }
        } else {
            /*
             * When creating a new item, first checks that none of 
             * required fields is empty. Otherwise gives warning 
             * messages as pop ups.
             */
            if(!itemNameField.getText().isEmpty()) {
                if(!itemSizeField.getText().isEmpty() && !itemWeightField.getText().isEmpty()) {
                    /*
                     * Tries to cast the strings from size and weight 
                     * TextFields to double. Gives a warning message 
                     * if cast fails.
                     *
                     * If cast successful, creates a new item with given
                     * data, then creates a package using the new item
                     * and the class chosen by the user.
                     */
                    try{
                        double size = Double.parseDouble(itemSizeField.getText());
                        double weight = Double.parseDouble(itemWeightField.getText());
                        
                        String name = itemNameField.getText();
                        boolean fragile = fragileCheck.isSelected();
                        
                        int chooseClass;
                        if(class1radio.isSelected()) {
                            chooseClass = 1;
                        } else if(class2radio.isSelected()) {
                            chooseClass = 2;
                        } else {
                            chooseClass = 3;
                        }
                        
                        /*
                         * Get the class' max size and weight for comparison.
                         */
                        double maxSize = 0;
                        double maxWeight = 0;
                        for (ClassInfo cinfo : classInfoList) {
                            if(cinfo.getClassID() == chooseClass) {
                                maxSize = cinfo.getClassSize();
                                maxWeight = cinfo.getClassWeight();
                                break;
                            }
                        }
                        
                        /*
                         * If the new item is within the size restrictions, 
                         * it can be created and packaged. Else throw a warning
                         * message.
                         */
                        if((maxSize == 0 && maxWeight == 0) || (maxSize >= size && maxWeight >= weight)) {
                            Item item = new Item(name, size, weight, fragile);
                            int itemID = item.getItemID();
                        
                            Package pkg = new Package(chooseClass, itemID);
                        
                            PackageStorage str = new PackageStorage();
                            str.addPackToStor(pkg);
                        
                            stage = (Stage) createAcceptButton.getScene().getWindow();
                            stage.close();
                        } else {
                            new createNewWindow("FXMLTooBig.fxml", "Warning", false);
                        }
                    } catch (NumberFormatException ex) {
                        new createNewWindow("FXMLNotDouble.fxml", "Warning", false);
                    }
                } else {
                    new createNewWindow("FXMLNoItemSize.fxml", "Warning", false);
                }
            } else {
                new createNewWindow("FXMLNoItemName.fxml", "Warning", false);
            }
        }
    }

    @FXML
    private void classInfoAction(ActionEvent event) {
        /*
         * Opens the class info window.
         */
        new createNewWindow("FXMLClassInfo.fxml", "Package classes", true);
    }

    @FXML
    private void showItemsAction(ActionEvent event) {
        /*
         * Opens the items in storage view.
         */
        new createNewWindow("FXMLItemsInfo.fxml", "Items", true);
    }
}
