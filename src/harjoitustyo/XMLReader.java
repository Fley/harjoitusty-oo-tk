/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: XMLReader.java
 * Author: Otto Itkonen
 */

/*
 * Takes a URL as a parameter and 
 * reads the XML it finds at the end of 
 * that URL, then saves the content it 
 * finds to a string line by line.
 */

package harjoitustyo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author k2002
 */
public class XMLReader {
    private String content;
    private URL url;
    private BufferedReader reader;
    private String line;
    
    public XMLReader(String address) {
        content = "";
        
        try {
            url = new URL(address);
            
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            
            while((line = reader.readLine()) != null) {
                content += line + "\n";
            }
        } catch(MalformedURLException e) {
            System.err.println("MalformedURLException caught: " + e.getMessage());
        } catch(IOException e) {
            System.err.println("IOException caught: " + e.getMessage());
        }
    }
    
    public String getXMLContent() {
        return content;
    }
}
