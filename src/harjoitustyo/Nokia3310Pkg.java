/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: Nokia3310Pkg.java
 * Author: Otto Itkonen
 */

/*
 * A class to package a Nokia 3310 mobile phone.
 */

package harjoitustyo;

/**
 *
 * @author k2002
 */
public class Nokia3310Pkg extends Package{

    public Nokia3310Pkg(int itemID) {
        super(1, itemID);
    }
}
