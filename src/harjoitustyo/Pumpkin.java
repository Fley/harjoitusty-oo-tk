/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: Pumpkin.java
 * Author: Otto Itkonen
 */

/*
 * A class to create a pumpkin.
 */

package harjoitustyo;

/**
 *
 * @author k2002
 */
public class Pumpkin extends Item {
    
    public Pumpkin() {
        super("Pumpkin", 3.5, 1.1, true);
    }
}
