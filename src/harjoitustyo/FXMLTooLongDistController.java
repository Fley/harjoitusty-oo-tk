/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: FXMLTooLongDistController.java
 * Author: Otto Itkonen
 */

/*
 * Controller for a warning window for if a package is tried to send
 * over too long distances for its class.
 */

package harjoitustyo;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author k2002
 */
public class FXMLTooLongDistController implements Initializable {
    @FXML
    private Button okButton;

    @Override
    public void initialize(URL url, ResourceBundle rb) {}    

    @FXML
    private void okButtonAction(ActionEvent event) {
        Stage stage = (Stage) okButton.getScene().getWindow();
        
        stage.close();
    }
    
}
