/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: Nokia3310.java
 * Author: Otto Itkonen
 */

/*
 * A class to create a Nokia 3310 mobile phone.
 */

package harjoitustyo;

/**
 *
 * @author k2002
 */
public class Nokia3310 extends Item {
    
    public Nokia3310() {
        super("Nokia 3310, mobile phone", 0.1, 0.2, false);
    }
}
