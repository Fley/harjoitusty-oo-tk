/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: GlassCannonPkg.java
 * Author: Otto Itkonen
 */

/*
 * A class to package a Glass Cannon. The only package 
 * class it fits in will definitely break it.
 */

package harjoitustyo;

/**
 *
 * @author k2002
 */
public class GlassCannonPkg extends Package{

    public GlassCannonPkg(int itemID) {
        super(3, itemID);
    }
}
