/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: FXMLDocumentController.java
 * Author: Otto Itkonen
 */

/*
 * The main window controller.
 */

package harjoitustyo;

import harjoitustyo.PackageStorage.PackageInfo;
import java.io.File;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author k2002
 */
public class FXMLDocumentController implements Initializable {
    
    private TextReader textReader;
    private String dbContent; // Database Content
    private XMLReader xmlReader;
    private XMLParser xmlParser;
    private SPDataExtractor dataExt;
    private String spURL; // URL for SmartPost XML
    private WebEngine mapEngine;
    private SmartPostStorage spStorage;
    private PackageStorage pkgStorage;
    private ItemStorage itemStorage;
    private PackageClassInfo pkgClassInfo;
    private boolean fragile;
    private boolean breakage;
    @FXML
    private AnchorPane backgroundPane;
    @FXML
    private Label addSPLabel;
    @FXML
    private ComboBox<String> cityBox;
    @FXML
    private Button addCityButton;
    @FXML
    private Label addPackageLabel;
    @FXML
    private Button createPackageButton;
    @FXML
    private ComboBox<PackageInfo> packageBox;
    @FXML
    private Label startLabel;
    @FXML
    private ComboBox<SmartPost> startBox;
    @FXML
    private Label destLabel;
    @FXML
    private ComboBox<SmartPost> destBox;
    @FXML
    private Button sendButton;
    @FXML
    private Button removeButton;
    @FXML
    private WebView mapView;
    @FXML
    private Button storageButton;
    @FXML
    private Label timoteiLabel;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        System.out.println("Starting program, please wait...");
        
        spStorage = new SmartPostStorage();
        pkgStorage = PackageStorage.newPkgStorage();
        itemStorage = new ItemStorage();
        
        /*
         * Creates a new XML reader and parser to
         * get all the required data for SmartPosts.
         */
        spURL = "http://smartpost.ee/fi_apt.xml";
        xmlReader = new XMLReader(spURL);
        xmlParser = new XMLParser(xmlReader.getXMLContent());
        
        /*
         * textReader just gets all the readable content
         * from the given database file, so it can be
         * used to form a new database.
         */
        textReader = new TextReader("smartpostdb.sql");
        dbContent = textReader.getTextContent();
        
        Connection con = null;
        Statement stat = null;
        
        /*
         * Checks for database existance and if it finds 
         * a database with the right name, deletes it.
         * It then proceeds to create a new database
         * with the same name. This ensures the database 
         * is always unedited and correct when starting 
         * the application without sending out unnecessary
         * exceptions.
         */
        File f = new File("timotei.db");
        if(f.exists())
            f.delete();
        
        try{
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:timotei.db");
            
            stat = con.createStatement();
            stat.executeUpdate(dbContent);
            
            stat.close();
            con.close();
        } catch(ClassNotFoundException e) {
            System.err.println("ClassNotFoundException caught: " + e.getMessage());
        } catch(SQLException e) {
            System.err.println("SQLException caught: " + e.getMessage());
        }
        
        /* 
         * Extracts the data XML parser got earlier and
         * inserts it into the newly created database.
         */
        dataExt = new SPDataExtractor(xmlParser.getParsedDocument());
        
        mapEngine = mapView.getEngine();
        mapEngine.load(getClass().getResource("index.html").toExternalForm());
        
        /*
         * Gets a list of cities with SmartPosts, sorts it
         * and adds all the cities into a ComboBox.
         */
        ArrayList<String> cities = SmartPostStorage.getCities();
        Collections.sort(cities);
        for (String city : cities) {
            cityBox.getItems().add(city);
        }
        
        /*
         * Gets package classes' info.
         */
        pkgClassInfo = new PackageClassInfo();
        
        /*
         * Some premade items and packages.
         */
        Nokia3310 neverBreaks = new Nokia3310();
        IkeaStool doItYourself = new IkeaStool();
        GlassCannon alwaysBreaks = new GlassCannon();
        Pumpkin pumpking = new Pumpkin();
        
        Nokia3310Pkg neverBreaksPkg = new Nokia3310Pkg(neverBreaks.getItemID());
        IkeaStoolPkg diyPkg = new IkeaStoolPkg(doItYourself.getItemID());
        GlassCannonPkg alwaysBreaksPkg = new GlassCannonPkg(alwaysBreaks.getItemID());
        PumpkinPkg pumpkingPkg = new PumpkinPkg(pumpking.getItemID());
        
        pkgStorage.addPackToStor(neverBreaksPkg);
        pkgStorage.addPackToStor(diyPkg);
        pkgStorage.addPackToStor(alwaysBreaksPkg);
        pkgStorage.addPackToStor(pumpkingPkg);
        
        updatePackageBox();
    }

    @FXML
    private void addCityAction(ActionEvent event) {
        /*
         * Adds city's SmartPosts on to the map.
         */
        if(cityBox.getValue() != null) {
            Connection con = null;
            Statement stat = null;
            ArrayList<SmartPost> spList = new ArrayList<>(); // List of SmartPosts
            
            String search = "SELECT * FROM SmartPost "
                    + "WHERE City = '" + cityBox.getValue() + "';";
            
            try {
                Class.forName("org.sqlite.JDBC");
                con = DriverManager.getConnection("jdbc:sqlite:timotei.db");
                con.setAutoCommit(false);
                
                stat = con.createStatement();
                ResultSet rs = stat.executeQuery(search);
                
                while(rs.next()) {
                    String address = rs.getString("Address");
                    String postalc = rs.getString("Postal code");
                    String city = rs.getString("City");
                    String office = rs.getString("Name of SmartPost");
                    String open = rs.getString("Post office open");
                    
                    /*
                     * Utilizes index.html to add the SmartPosts.
                     */
                    mapEngine.executeScript("document.goToLocation('"
                            + address + ", " + postalc + " " + city + "', '"
                            + office + ", " + open + "', 'red')");
                    
                    /*
                     * Create a new SmartPost-object and add it to spList
                     * so that they can be added to the onMap list later.
                     */
                    SmartPost sp = new SmartPost(postalc, city, address, open, office);
                    spList.add(sp);
                }
                
                con.commit();
                con.setAutoCommit(true);
                
                rs.close();
                stat.close();
                con.close();
            } catch(ClassNotFoundException e) {
                System.err.println("ClassNotFoundException caught: " + e.getMessage());
            } catch(SQLException e) {
                System.err.println("SQLException caught: " + e.getMessage());
            }
            
            /*
             * Tests if the SmartPosts of a city are already on the 
             * SPsOnMap list. If not, they are added on the list.
             */
            boolean isOnMap = false;
            for (SmartPost sp : SmartPostStorage.getSPsOnMap()) {
                if(spList.get(0).getAddress().equals(sp.getAddress()) && 
                        spList.get(0).getPostCode().equals(sp.getPostCode())) {
                    isOnMap = true;
                    break;
                }
            }
            
            if(isOnMap == false) {
                for (SmartPost sp : spList) {
                    SmartPostStorage.addToSPsOnMap(sp);
                }
            }
            
            /*
             * The onMap ArrayList gets SPsOnMap list from SmartPostStorage
             * so that it can be used to update startBox and destBox
             * contents to include the SPs newly added to the map.
             */
            ArrayList<SmartPost> onMap = SmartPostStorage.getSPsOnMap();
            
            startBox.getItems().clear();
            destBox.getItems().clear();
            
            startBox.getItems().addAll(onMap);
            destBox.getItems().addAll(onMap);
        }
    }

    @FXML
    private void createPackageAction(ActionEvent event) {
        /*
         * Use createNewWindow to open the package creator.
         * Listens to the window closing, so it can update the 
         * ComboBox packageBox when possible changes happen.
         */
        createNewWindow pCreatorWindow; 
        pCreatorWindow = new createNewWindow("FXMLPackageCreator.fxml", "Package creation", false);
        
        Stage stage = pCreatorWindow.getStage();
        
        stage.setOnHidden(new EventHandler<WindowEvent>() {
            @Override
            public void handle (WindowEvent we) {
                updatePackageBox();
            }
        });
    }

    @FXML
    private void sendAction(ActionEvent event) {
        /*
         * Checks whether user has given the required information and
         * sends the package.
         */
        if(packageBox.getValue() != null) {
            if(startBox.getValue() != null && destBox.getValue() != null) {
                PackageInfo pkg = packageBox.getValue();
                SmartPost start = startBox.getValue();
                SmartPost dest = destBox.getValue();
                
                int pkgID = pkg.getPackageID();
                int pkgClass = pkg.getPackageClass();
                
                Object distance;
                double dist = 0;
                
                ArrayList paths = new ArrayList();
                
                Connection con = null;
                Statement stat = null;

                /*
                 * SQL commands for searching for starting point's and
                 * destination's SmartPost IDs and to delete the sent 
                 * package from inventory. Also checks whether the
                 * package breaks on its way.
                 */
                String searchStart = "SELECT ID FROM AddressAndID "
                        + "WHERE Address = '" + start.getAddress() + "' AND "
                        + "City = '" + start.getCity() + "' AND "
                        + "Code = '" + start.getPostCode() + "';";
                
                String searchDest = "SELECT ID FROM AddressAndID "
                        + "WHERE Address = '" + dest.getAddress() + "' AND "
                        + "City = '" + dest.getCity() + "' AND "
                        + "Code = '" + dest.getPostCode() + "';";
                
                String pkgBroke = "SELECT * FROM PackageBreak "
                        + "WHERE ID = " + pkgID + ";";
                
                String sendPkg = "DELETE FROM Package WHERE packageid = "
                        + pkgID + ";";
                
                try {
                    Class.forName("org.sqlite.JDBC");
                    con = DriverManager.getConnection("jdbc:sqlite:timotei.db");
                    con.setAutoCommit(false);
                    
                    stat = con.createStatement();
                    
                    /*
                     * Gets starting point's coordinates and adds into the
                     * ArrayList paths which is reserved for SP coordinates.
                     */
                    ResultSet startSet = stat.executeQuery(searchStart);
                    int startID = startSet.getInt("ID");
                    startSet.close();
                    
                    String coordStart = "SELECT * FROM Coordinates "
                            + "WHERE smartpostid = " + startID + ";";
                    
                    ResultSet startCoordSet = stat.executeQuery(coordStart);
                    double latStart = startCoordSet.getDouble("latitude");
                    double lonStart = startCoordSet.getDouble("longitude");
                    paths.add(latStart);
                    paths.add(lonStart);
                    startCoordSet.close();
                    
                    /*
                     * Gets destination's coordinates and adds into the
                     * paths ArrayList.
                     */
                    ResultSet destSet = stat.executeQuery(searchDest);
                    int destID = destSet.getInt("ID");
                    destSet.close();
                    
                    String coordDest = "SELECT * FROM Coordinates "
                            + "WHERE smartpostid = " + destID + ";";
                    
                    ResultSet destCoordSet = stat.executeQuery(coordDest);
                    double latDest = destCoordSet.getDouble("latitude");
                    double lonDest = destCoordSet.getDouble("longitude");
                    paths.add(latDest);
                    paths.add(lonDest);
                    destCoordSet.close();
                    
                    /*
                     * Execute a script command to find out the distance between
                     * starting point and destination, then use that distance
                     * to determine whether or not it's too long for some classes.
                     */
                    distance = mapEngine.executeScript("document.pathDistance(" + paths + ")");
                    dist = Double.parseDouble(distance.toString());
                    
                    /*
                     * Gets package's fragility and package class'
                     * danger of item breakage.
                     */
                    ResultSet breakSet = stat.executeQuery(pkgBroke);
                    fragile = breakSet.getBoolean("Fragile");
                    breakage = breakSet.getBoolean("Breakage");
                    breakSet.close();
                    
                    /*
                     * Chooses path color and speed depending on which 
                     * class the package is sent in. The speeds are slightly
                     * faster than the original 1, 2 and 3 instructed in
                     * the assignment.
                     * 
                     * If 1st class delivery is chosen, if-clause checks the
                     * distance between SmartPosts and lets user know with
                     * a pop up if the distance is too long (over 150 km).
                     */
                    switch(pkgClass) {
                        case 1:     if(dist <= 150) {
                                        mapEngine.executeScript("document.createPath(" + paths 
                                                + ", 'green', 0.4)");
                                        stat.executeUpdate(sendPkg);
                                    } else {
                                        new createNewWindow("FXMLTooLongDist.fxml", "Warning", false);
                                    }
                                    break;
                        case 2:     mapEngine.executeScript("document.createPath(" + paths 
                                            + ", 'blue', 1.8)");
                                    stat.executeUpdate(sendPkg);
                                    break;
                        case 3:     mapEngine.executeScript("document.createPath(" + paths 
                                            + ", 'red', 1.0)");
                                    stat.executeUpdate(sendPkg);
                                    break;
                        default:    mapEngine.executeScript("document.createPath(" + paths 
                                            + ", 'red', 1.0)");
                                    stat.executeUpdate(sendPkg);
                                    break;
                    }
                    
                    con.commit();
                    con.setAutoCommit(true);
                    
                    stat.close();
                    con.close();
                } catch(ClassNotFoundException e) {
                    System.err.println("ClassNotFoundException caught: " + e.getMessage());
                } catch(SQLException e) {
                    System.err.println("SQLException caught: " + e.getMessage());
                }
                
                /*
                 * Updates the packageBox to reflect the new state
                 * of the package storage, now that one package has been
                 * sent and therefore deleted from the system.
                 */
                updatePackageBox();
                packageBox.setValue(null);
                
                /*
                 * Gives a notification if sent package broke.
                 */
                if(pkgClass != 1) {
                    if(fragile == breakage)
                        new createNewWindow("FXMLBroken.fxml", "Attention!", false);
                } else if(dist <= 150) {
                    if(fragile == breakage)
                        new createNewWindow("FXMLBroken.fxml", "Attention!", false);
                }
            } else {
                new createNewWindow("FXMLNoPlace.fxml", "Warning", false);
            }
        } else {
            new createNewWindow("FXMLNoPackage.fxml", "Warning", false);
        }
    }

    @FXML
    private void removeAllAction(ActionEvent event) {
        /*
         * Utilizes index.html to delete all visible paths from the map.
         */
        mapEngine.executeScript("document.deletePaths()");
    }

    @FXML
    private void showStorageAction(ActionEvent event) {
        /*
         * Use createNewWindow to open an info window of storaged packages.
         */
        new createNewWindow("FXMLStorageInfo.fxml", "Storaged items", true);
    }
    
    private void updatePackageBox() {
        /*
         * Clears packageBox's contents and fills it with updated storage.
         */
        packageBox.getItems().clear();
        packageBox.getItems().addAll(pkgStorage.getUpdatedStorage());
    }
    
}
