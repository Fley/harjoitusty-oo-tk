/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: FXMLStorageInfoController.java
 * Author: Otto Itkonen
 */

/*
 * Controller for the package info view.
 */

package harjoitustyo;

import harjoitustyo.PackageStorage.PackageInfoExtended;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;

/**
 * FXML Controller class
 *
 * @author k2002
 */
public class FXMLStorageInfoController implements Initializable {
    
    private PackageStorage pkgStorage;
    private ArrayList<PackageInfoExtended> storaged;
    private String content;
    @FXML
    private Label headerLabel;
    @FXML
    private TextArea infoTextArea;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        pkgStorage = new PackageStorage();
        
        storaged = pkgStorage.getUpdatedExtendedStorage();
        
        infoTextArea.setEditable(false);
        infoTextArea.setWrapText(false);
        
        content = "";
        
        for (PackageInfoExtended pix : storaged) {
            content = content + pix.toString() + "\n\n";
        }
        
        infoTextArea.setText(content);
    }    

}
