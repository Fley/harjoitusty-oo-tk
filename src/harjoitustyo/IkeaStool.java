/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: IkeaStool.java
 * Author: Otto Itkonen
 */

/*
 * A class to create a stool from Ikea.
 */

package harjoitustyo;

/**
 *
 * @author k2002
 */
public class IkeaStool extends Item {
    
    public IkeaStool() {
        super("Stool, Ikea", 3.7, 1.2, false);
    }
}
