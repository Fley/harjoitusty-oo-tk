/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: FXMLClassInfoController.java
 * Author: Otto Itkonen
 */

/*
 * Controller for the classes' info view.
 */

package harjoitustyo;

import harjoitustyo.PackageClassInfo.ClassInfo;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;

/**
 * FXML Controller class
 *
 * @author k2002
 */
public class FXMLClassInfoController implements Initializable {
    
    private ArrayList<ClassInfo> classes;
    private String content;
    @FXML
    private Label headerLabel;
    @FXML
    private TextArea infoTextArea;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        classes = PackageClassInfo.getClassInfo();
        
        infoTextArea.setEditable(false);
        infoTextArea.setWrapText(false);
        
        content = "";
        
        for (ClassInfo ci : classes) {
            content = content + ci.toString() + "\n\n";
        }
        
        infoTextArea.setText(content);
    }    
    
}
