/*
 * Program name: TIMOTEI, oo/tk harjoitustyö
 * File name: Mainclass.java
 * Author: Otto Itkonen
 */

/*
 * The Mainclass of the project. Used only for launching the application.
 */

package harjoitustyo;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author k2002
 */
public class Mainclass extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        
        Stage mainWindow = new Stage();
        Scene mainScene = new Scene(root);
        
        mainWindow.setTitle("TIMOTEI Package Management System");
        mainWindow.setScene(mainScene);
        mainWindow.show();
        mainWindow.centerOnScreen();
        mainWindow.toFront();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
