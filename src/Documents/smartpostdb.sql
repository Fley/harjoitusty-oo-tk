PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE "Item" (
    "itemid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    "itemname" VARCHAR(64) NOT NULL,
    "itemsize" DOUBLE NOT NULL,
    "itemweight" DOUBLE NOT NULL,
    "fragile" BOOLEAN DEFAULT (1)
);
CREATE TABLE "Package" (
    "packageid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    "itemid" INTEGER NOT NULL,
    "classid" INTEGER NOT NULL,
  
    FOREIGN KEY("itemid") REFERENCES "Item"("itemid"),
    FOREIGN KEY("classid") REFERENCES "Class"("classid")
);
CREATE TABLE "Class" (
    "classid" INTEGER PRIMARY KEY NOT NULL,
    "maxweight" DOUBLE,
    "maxsize" DOUBLE,
    "maxdistance" INTEGER,
  
    CHECK ("classid" IN (1, 2, 3))
);
INSERT INTO "Class" VALUES(1,3.0,4.0,150);
INSERT INTO "Class"("classid", "maxweight", "maxsize") VALUES(2,5.0,5.0);
INSERT INTO "Class"("classid") VALUES(3);
CREATE TABLE "Delivery" (
    "classid" INTEGER NOT NULL,
    "speed" INTEGER NOT NULL,
    "breakage" BOOLEAN DEFAULT (1),
  
    PRIMARY KEY("classid"),
    FOREIGN KEY("classid") REFERENCES "Class"("classid")
);
INSERT INTO "Delivery" VALUES(1,1,1);
INSERT INTO "Delivery" VALUES(2,7,0);
INSERT INTO "Delivery" VALUES(3,3,1);
CREATE TABLE "Coordinates" (
    "smartpostid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    "latitude" DOUBLE NOT NULL,
    "longitude" DOUBLE NOT NULL
);
CREATE TABLE "Info" (
    "smartpostid" INTEGER NOT NULL,
    "postname" VARCHAR(64) NOT NULL,
    "postopen" VARCHAR(64) NOT NULL,
  
    PRIMARY KEY("smartpostid"),
    FOREIGN KEY("smartpostid") REFERENCES "Coordinates"("smartpostid")
);
CREATE TABLE "Address" (
    "smartpostid" INTEGER NOT NULL,
    "address" VARCHAR(64) NOT NULL,
  
    PRIMARY KEY("smartpostid"),
    FOREIGN KEY("smartpostid") REFERENCES "Coordinates"("smartpostid")
);
CREATE TABLE "Postcode" (
    "smartpostid" INTEGER NOT NULL,
    "postalcode" VARCHAR(8) NOT NULL,
  
    PRIMARY KEY("smartpostid"),
    FOREIGN KEY("smartpostid") REFERENCES "Coordinates"("smartpostid")
);
CREATE TABLE "City" (
    "postalcode" VARCHAR(8) NOT NULL,
    "city" VARCHAR(16) NOT NULL,
  
    PRIMARY KEY("postalcode"),
    FOREIGN KEY("postalcode") REFERENCES "Postcode"("postalcode")
);
CREATE VIEW "ItemID" AS
    SELECT "itemid" AS "ID", 
    "itemname" AS "Name"
    FROM "Item";
CREATE VIEW "Storaged" AS
    SELECT "Package"."packageid" AS "Package number",
    "Class"."classid" AS "Class",
    "Item"."itemname" AS "Item"
    FROM "Item" INNER JOIN "Package" ON "Item"."itemid" = "Package"."itemid"
    INNER JOIN "Class" ON "Package"."classid" = "Class"."classid";
CREATE VIEW "StoragedExtended" AS
    SELECT "Package"."packageid" AS "Package number",
    "Class"."classid" AS "Class",
    "Item"."itemname" AS "Item",
    "Item"."itemsize" AS "Size",
    "Item"."itemweight" AS "Weight"
    FROM "Item" INNER JOIN "Package" ON "Item"."itemid" = "Package"."itemid"
    INNER JOIN "Class" ON "Package"."classid" = "Class"."classid";
CREATE VIEW "PackageBreak" AS
    SELECT "Package"."packageid" AS "ID",
    "Item"."fragile" AS "Fragile",
    "Delivery"."breakage" AS "Breakage"
    FROM "Package" INNER JOIN "Item" ON "Package"."itemid" = "Item"."itemid"
    INNER JOIN "Class" ON "Package"."classid" = "Class"."classid"
    INNER JOIN "Delivery" ON "Class"."classid" = "Delivery"."classid"
    ORDER BY "Package"."packageid";
CREATE VIEW "ClassInfo" AS
    SELECT "Class"."classid" AS "Class",
    "Delivery"."speed" AS "Delivery",
    "Class"."maxsize" AS "MaxSize",
    "Class"."maxweight" AS "MaxWeight",
    "Class"."maxdistance" AS "MaxDistance",
    "Delivery"."breakage" AS "Breakage"
    FROM "Class" INNER JOIN "Delivery" ON "Class"."classid" = "Delivery"."classid"
    ORDER BY "Class"."classid";
CREATE VIEW "SmartPost" AS
    SELECT "Info"."postname" AS "Name of SmartPost",
    "Address"."address" AS "Address",
    "Postcode"."postalcode" AS "Postal code",
    "City"."city" AS "City",
    "Info"."postopen" AS "Post office open"
    FROM "Info" INNER JOIN "Address" ON "Info"."smartpostid" = "Address"."smartpostid"
    INNER JOIN "Postcode" ON "Info"."smartpostid" = "Postcode"."smartpostid"
    INNER JOIN "City" ON "Postcode"."postalcode" = "City"."postalcode"
    ORDER BY "City"."city", "Postcode"."postalcode", "Info"."postname";
CREATE VIEW "AddressAndID" AS
    SELECT "Coordinates"."smartpostid" AS "ID",
    "Address"."address" AS "Address",
    "Postcode"."postalcode" AS "Code",
    "City"."city" AS "City"
    FROM "Coordinates" INNER JOIN "Address" ON "Coordinates"."smartpostid" = "Address"."smartpostid"
    INNER JOIN "Postcode" ON "Coordinates"."smartpostid" = "Postcode"."smartpostid"
    INNER JOIN "City" ON "Postcode"."postalcode" = "City"."postalcode"
    ORDER BY "Coordinates"."smartpostid";
COMMIT;
