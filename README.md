# Repositorio olio-ohjelmoinnin ja tietokantojen harkkatyötä varten. #

* Opiskelija: Otto Itkonen 
* Op.num: 0438426

 
### Linkki demovideoon: [https://youtu.be/gEN_vu0nIE8](https://youtu.be/gEN_vu0nIE8) ###
Pahoittelen demovideon äänen hiljaisuutta, se johtuu huonosta mikistäni. 


## Ohjelman käyttö ##
Ohjelma toimii parhaiten komentorivin kautta käynnistettynä Java 8:lla. Ohjelma käynnistyy myös, jos käytössä on NetBeans 8.0 ja Java 7, mutta kartta ei toimi kunnolla ja liikkuu ainoastaan nuolinäppäimillä. Ohjelmaa on testattu ja käytetty yliopiston Ubuntu-koneella.

Jotta ohjelma toimisi oikein komentorivin kautta, on projektikansion "dist"-kansiossa oltava tietokanta-tiedosto "smartpostdb.sql". Mikäli tiedostoa ei siis kansiossa muuten ole, se on kopioitava sinne joko pääkansiosta tai dokumentaatiokansiosta.

Dokumentaatio löytyy pääkansion sisältä polulta "src/Documents". Kansion nimi on siis "Documents" ja se on kansion "harjoitustyo" vieressä.